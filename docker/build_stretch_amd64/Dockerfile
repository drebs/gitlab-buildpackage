FROM 0xacab.org:4567/leap/docker/debian:stretch_amd64

MAINTAINER LEAP Encryption Access Project <sysdev@leap.se>

ENV DEBIAN_FRONTEND noninteractive

# Add docker repo
RUN echo 'deb https://apt.dockerproject.org/repo debian-stretch main'> /etc/apt/sources.list.d/docker.list
RUN curl -s https://apt.dockerproject.org/gpg | apt-key add -

# Add nodejs LTS repo
RUn echo 'deb https://deb.nodesource.com/node_8.x stretch main' > /etc/apt/sources.list.d/nodesource.list
RUN curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -

RUN apt-get update && apt-get -y dist-upgrade -y && apt-get install -y --no-install-recommends \
equivs openssh-client pep8 build-essential devscripts dpkg-dev fakeroot git-buildpackage \
lintian pristine-tar runit aptitude ca-certificates apt-utils libdistro-info-perl dh-systemd \
python-all python-setuptools python-wheel \
curl gnupg docker-engine \
nodejs

RUN rm -rf /var/lib/apt/lists/*

RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN adduser --group --system build
RUN git config --global user.email 'sysdev@leap.se' && git config --global user.name "Automatic build"

COPY common/gbp-envdir /usr/local/gbp-envdir/
COPY common/etc-extras /etc/
COPY common/build-scripts /usr/local/sbin/
