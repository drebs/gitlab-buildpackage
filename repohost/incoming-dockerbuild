#!/bin/bash

# Usage:
#
#    incoming_dockerbuild <INCOMINGDIR> <REPOSITORY> <DIST>

set -e

# Copy/symlink common/build-scripts/build-lib of this repo
# to /usr/local/sbin/build-lib on the on the repohost
. /usr/local/sbin/build-lib

# shellcheck disable=SC1090
. "${HOME}"/custom-vars

# Source custom config file if present
# shellcheck source=/dev/null
[ -e "$HOME/.gitlab-buildpackagerc" ] && source "$HOME/.gitlab-buildpackagerc"

INCOMINGDIR="$1"
export REPOSITORY="$2"
export DIST="$3"

if [ ! -d "$INCOMINGDIR" ]; then
  echo "Directory $INCOMINGDIR is not a directory."
  exit 1
fi

if [ -z "$REPOSITORY" ]; then
  echo "\$REPOSITORY is not provided."
  exit 1
fi

if [ -z "$DIST" ]; then
  echo "\$DIST is not provided."
  exit 1
fi

if [ $(id -u -n) != "gitlab-runner" ]; then
  chmod 755 -- "$INCOMINGDIR"
  sudo -n -u gitlab-runner "$0" "$@"
  RC=$?
  rm -rf -- "$INCOMINGDIR"
  exit $RC
fi

read REPOS < "$INCOMINGDIR"/target-distribution
if [ -z "$REPOS" ]; then
  echo "E: target-distribution was empty"
  exit 1
fi

generate-reprepro-codename "${DIST}"

. /etc/jenkins/debian_glue

SOURCE_PACKAGE=$(basename "$INCOMINGDIR"/*.changes | awk -F_ '{ print $1 }')

reprepro -C "$REPOS" -A source -b "${REPOSITORY}" ${REPREPRO_OPTS} remove "${DIST}" "${SOURCE_PACKAGE}"

REPREPRO_OPTS='-v --waitforlock 1000'
echo "*** Including packages in repository $REPOSITORY, codename $DIST, component $REPOS ***"

echo "GPG_AGENT_INFO: $GPG_AGENT_INFO"

reprepro -C "$REPOS" -b "${REPOSITORY}" \
    --ignore=wrongdistribution --ignore=uploaders --ignore=surprisingbinary \
    include "${DIST}" "$INCOMINGDIR"/*.changes
