#!/bin/sh

VERSION_FILE="src/leap/mx/_version.py"
[ -f ${VERSION_FILE} ] && rm ${VERSION_FILE}
echo y | python setup.py freeze_debianver

# Add the number of commits after last tag to the version string
#commits_since_last_tag=$(git rev-list "$(git rev-list --tags --no-walk --max-count=1)"..HEAD --count)
#rev=$(git rev-parse --short HEAD)
#sed -i "/ \"version/s/\"$/.${commits_since_last_tag}.g${rev}\"/" ${VERSION_FILE}

# Take version from last debian/changelog entry
dch_version=$(head -1 debian/changelog | grep -o '(.*)' | tr -d '()' | tr '+~' '.')
sed -i "/ \"version/s/.*/ \"version\": \"${dch_version}\"/" ${VERSION_FILE}
# Remove the -dirty tag
sed -i 's/-dirty//g' ${VERSION_FILE}

git commit -m "[pkg] freeze debian version" ${VERSION_FILE}

python setup.py version
